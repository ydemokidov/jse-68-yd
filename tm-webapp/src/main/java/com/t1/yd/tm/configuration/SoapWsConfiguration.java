package com.t1.yd.tm.configuration;

import com.t1.yd.tm.endpoint.ProjectSoapEndpoint;
import com.t1.yd.tm.endpoint.TaskSoapEndpoint;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.config.annotation.WsConfigurerAdapter;
import org.springframework.ws.server.EndpointInterceptor;
import org.springframework.ws.server.endpoint.mapping.PayloadRootAnnotationMethodEndpointMapping;
import org.springframework.ws.soap.server.endpoint.SoapFaultAnnotationExceptionResolver;
import org.springframework.ws.soap.server.endpoint.interceptor.SoapEnvelopeLoggingInterceptor;
import org.springframework.ws.transport.http.MessageDispatcherServlet;
import org.springframework.ws.wsdl.wsdl11.DefaultWsdl11Definition;
import org.springframework.xml.xsd.SimpleXsdSchema;
import org.springframework.xml.xsd.XsdSchema;

@EnableWs
@Configuration
public class SoapWsConfiguration extends WsConfigurerAdapter {

    @Bean
    public SoapEnvelopeLoggingInterceptor soapEnvelopeLoggingInterceptor() {
        return new SoapEnvelopeLoggingInterceptor();
    }

    @Bean
    public PayloadRootAnnotationMethodEndpointMapping payloadRootAnnotationMethodEndpointMapping() {
        PayloadRootAnnotationMethodEndpointMapping mapping = new PayloadRootAnnotationMethodEndpointMapping();
        mapping.setInterceptors(new EndpointInterceptor[]{soapEnvelopeLoggingInterceptor()});
        return mapping;
    }

    @Bean
    public SoapFaultAnnotationExceptionResolver exceptionResolver() {
        return new SoapFaultAnnotationExceptionResolver();
    }

    @Bean(name = "projects")
    public DefaultWsdl11Definition projectWsdl11Definition(final XsdSchema projectEndpointSchema) {
        return buildWsdlFromSchema(projectEndpointSchema, "projectPort", "/ws", ProjectSoapEndpoint.NAMESPACE_URI);
    }

    @Bean(name = "tasks")
    public DefaultWsdl11Definition taskWsdl11Definition(final XsdSchema taskEndpointSchema) {
        return buildWsdlFromSchema(taskEndpointSchema, "taskPort", "/ws", TaskSoapEndpoint.NAMESPACE_URI);
    }

    private DefaultWsdl11Definition buildWsdlFromSchema(XsdSchema schema, String port, String location, String namespace) {
        DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
        wsdl11Definition.setPortTypeName(port);
        wsdl11Definition.setLocationUri(location);
        wsdl11Definition.setTargetNamespace(namespace);
        wsdl11Definition.setSchema(schema);
        return wsdl11Definition;
    }

    @Bean
    public ServletRegistrationBean messageDispatcherServlet(ApplicationContext applicationContext) {
        MessageDispatcherServlet messageDispatcherServlet = new MessageDispatcherServlet();
        messageDispatcherServlet.setApplicationContext(applicationContext);
        messageDispatcherServlet.setTransformWsdlLocations(true);
        return new ServletRegistrationBean(messageDispatcherServlet, "/ws/*");
    }

    @Bean
    public XsdSchema projectEndpointSchema() {
        return new SimpleXsdSchema(new ClassPathResource("schema/ProjectDataContract.xsd"));
    }

    @Bean
    public XsdSchema taskEndpointSchema() {
        return new SimpleXsdSchema(new ClassPathResource("schema/TaskDataContract.xsd"));
    }


}
