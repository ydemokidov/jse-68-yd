package com.t1.yd.tm.service;

import com.t1.yd.tm.api.service.IProjectDTOService;
import com.t1.yd.tm.dto.model.ProjectDTO;
import com.t1.yd.tm.enumerated.Status;
import com.t1.yd.tm.exception.entity.ProjectNotFoundException;
import com.t1.yd.tm.exception.field.DescriptionEmptyException;
import com.t1.yd.tm.exception.field.IdEmptyException;
import com.t1.yd.tm.exception.field.NameEmptyException;
import com.t1.yd.tm.repository.ProjectDTORepository;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class ProjectDTOService implements IProjectDTOService {

    @NotNull
    private final ProjectDTORepository repository;

    @Autowired
    public ProjectDTOService(@NotNull final ProjectDTORepository repository) {
        this.repository = repository;
    }

    @NotNull
    @Override
    @Transactional
    public ProjectDTO add(@NotNull final ProjectDTO project) throws Exception {
        return repository.save(project);
    }

    @Override
    @Transactional
    public void clear() throws Exception {
        repository.deleteAll();
    }

    @Override
    public boolean existsById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) {
            return false;
        }
        return repository.existsById(id);
    }

    @Override
    public @NotNull List<ProjectDTO> findAll() throws Exception {
        return repository.findAll();
    }

    @Nullable
    @Override
    public ProjectDTO findOneById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) {
            throw new IdEmptyException();
        }
        return repository.findById(id).orElse(null);
    }

    @Override
    public int count() throws Exception {
        return (int) repository.count();
    }

    @Override
    @Transactional
    public void remove(@Nullable final ProjectDTO project) throws Exception {
        if (project == null) {
            return;
        }
        repository.delete(project);
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) {
            throw new IdEmptyException();
        }
        repository.deleteById(id);
    }

    @Override
    @Transactional
    public ProjectDTO update(@Nullable final ProjectDTO project) throws Exception {
        if (project == null) {
            return null;
        }
        return repository.save(project);
    }

    @Override
    @Transactional
    public void changeProjectStatusById(@Nullable final String id, @Nullable final Status status) throws Exception {
        if (id == null || id.isEmpty()) {
            throw new IdEmptyException();
        }
        @Nullable final ProjectDTO project = findOneById(id);
        if (project == null) {
            throw new ProjectNotFoundException();
        }
        project.setStatus(status);
        repository.save(project);
    }

    @NotNull
    @Override
    @Transactional
    public ProjectDTO create(@Nullable final String name) throws Exception {
        if (name == null || name.isEmpty()) {
            throw new NameEmptyException();
        }
        @NotNull ProjectDTO project = new ProjectDTO();
        project.setName(name);
        return repository.save(project);
    }

    @NotNull
    @Override
    @Transactional
    public ProjectDTO create(
            @Nullable final String name,
            @Nullable final String description
    ) throws Exception {
        if (name == null || name.isEmpty()) {
            throw new NameEmptyException();
        }
        if (description == null || description.isEmpty()) {
            throw new DescriptionEmptyException();
        }
        @NotNull ProjectDTO project = new ProjectDTO();
        project.setName(name);
        project.setDescription(description);
        return repository.save(project);
    }

    @Override
    @Transactional
    public void updateById(@Nullable final String id, @Nullable final String name, @Nullable final String description) throws Exception {
        if (id == null || id.isEmpty()) {
            throw new IdEmptyException();
        }
        if (name == null || name.isEmpty()) {
            throw new NameEmptyException();
        }
        if (description == null || description.isEmpty()) {
            throw new DescriptionEmptyException();
        }
        @Nullable final ProjectDTO project = findOneById(id);
        if (project == null) {
            throw new ProjectNotFoundException();
        }
        project.setName(name);
        project.setDescription(description);
        repository.save(project);
    }

}
