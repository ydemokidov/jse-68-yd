package com.t1.yd.tm.endpoint;

import com.t1.yd.tm.api.service.ITaskDTOService;
import com.t1.yd.tm.dto.model.TaskDTO;
import com.t1.yd.tm.task.schemas.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import javax.validation.constraints.NotNull;
import java.math.BigInteger;
import java.util.stream.Collectors;

@Endpoint
public class TaskSoapEndpoint {

    public static final String NAMESPACE_URI = "http://t1.com/yd/tm/task/schemas";

    private final ITaskDTOService taskDTOService;

    @Autowired
    public TaskSoapEndpoint(@NotNull final ITaskDTOService taskDTOService) {
        this.taskDTOService = taskDTOService;
    }

    @ResponsePayload
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "TaskListRequest")
    public TaskListResponse findAllTasks(@RequestPayload @NotNull final TaskListRequest request) throws Exception {
        final TaskListResponse response = new TaskListResponse();
        response.getTasks().addAll(taskDTOService.findAll().stream()
                .map(this::taskDTOToTaskForSoap)
                .collect(Collectors.toList()));
        return response;
    }

    @ResponsePayload
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "TaskByProjectIdRequest")
    public TaskListResponse findAllTasksByProjectId(@RequestPayload @NotNull final TaskByProjectIdRequest request) throws Exception {
        final TaskListResponse response = new TaskListResponse();
        response.getTasks().addAll(taskDTOService.findAllByProjectId(request.getProjectId()).stream()
                .map(this::taskDTOToTaskForSoap)
                .collect(Collectors.toList()));
        return response;
    }

    @ResponsePayload
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "TaskDeleteByIdRequest")
    public TaskDeleteByIdResponse deleteTaskById(@RequestPayload @NotNull final TaskDeleteByIdRequest request) throws Exception {
        final TaskDTO taskDTO = taskDTOService.findOneById(request.getId());
        taskDTOService.removeById(request.getId());
        final Task task = taskDTOToTaskForSoap(taskDTO);
        final TaskDeleteByIdResponse response = new TaskDeleteByIdResponse();
        response.setTask(task);
        return response;
    }

    @ResponsePayload
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "TaskDeleteRequest")
    public TaskDeleteResponse deleteTask(@RequestPayload @NotNull final TaskDeleteRequest request) throws Exception {
        final TaskDTO taskDTO = taskDTOService.findOneById(request.getTask().getId());
        taskDTOService.remove(taskDTO);
        final Task task = taskDTOToTaskForSoap(taskDTO);
        final TaskDeleteResponse response = new TaskDeleteResponse();
        response.setTask(task);
        return response;
    }

    @ResponsePayload
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "TaskByIdRequest")
    public TaskByIdResponse findTaskById(@RequestPayload @NotNull final TaskByIdRequest request) throws Exception {
        final TaskDTO taskDTO = taskDTOService.findOneById(request.getId());
        final TaskByIdResponse response = new TaskByIdResponse();
        response.setTask(taskDTOToTaskForSoap(taskDTO));
        return response;
    }

    @ResponsePayload
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "TaskUpdateRequest")
    public TaskUpdateResponse updateTask(@RequestPayload @NotNull final TaskUpdateRequest request) throws Exception {
        final TaskDTO taskDTO = taskDTOService.findOneById(request.getTask().getId());
        taskDTO.setName(request.getTask().getName());
        taskDTO.setDescription(request.getTask().getDescription());
        taskDTO.setProjectId(request.getTask().getProjectId());
        taskDTOService.update(taskDTO);
        final TaskUpdateResponse response = new TaskUpdateResponse();
        response.setTask(request.getTask());
        return response;
    }

    @ResponsePayload
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "TaskCountRequest")
    public TaskCountResponse getTaskCount(@RequestPayload @NotNull final TaskCountRequest request) throws Exception {
        final TaskCountResponse response = new TaskCountResponse();
        response.setCount(BigInteger.valueOf(taskDTOService.count()));
        return response;
    }

    private Task taskDTOToTaskForSoap(@NotNull final TaskDTO taskDTO) {
        final Task task = new Task();
        task.setId(taskDTO.getId());
        task.setName(taskDTO.getName());
        task.setDescription(taskDTO.getDescription());
        task.setProjectId(taskDTO.getProjectId());
        return task;
    }

}
