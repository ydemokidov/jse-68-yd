package com.t1.yd.tm.endpoint;

import com.t1.yd.tm.api.service.IProjectDTOService;
import com.t1.yd.tm.dto.model.ProjectDTO;
import com.t1.yd.tm.project.schemas.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import javax.validation.constraints.NotNull;
import java.math.BigInteger;
import java.util.stream.Collectors;

@Endpoint
public class ProjectSoapEndpoint {

    public static final String NAMESPACE_URI = "http://t1.com/yd/tm/project/schemas";

    private final IProjectDTOService projectDTOService;

    @Autowired
    public ProjectSoapEndpoint(@NotNull final IProjectDTOService projectDTOService) {
        this.projectDTOService = projectDTOService;
    }

    @ResponsePayload
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "ProjectListRequest")
    public ProjectListResponse findAllProjects(@RequestPayload @NotNull final ProjectListRequest request) throws Exception {
        final ProjectListResponse response = new ProjectListResponse();
        response.getProject().addAll(projectDTOService.findAll().stream()
                .map(this::projectDTOToProjectForSoap).collect(Collectors.toList()));
        return response;
    }

    @ResponsePayload
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "ProjectDeleteByIdRequest")
    public ProjectDeleteByIdResponse deleteProjectById(@RequestPayload @NotNull final ProjectDeleteByIdRequest request) throws Exception {
        final ProjectDTO projectDTO = projectDTOService.findOneById(request.getId());
        projectDTOService.removeById(request.getId());
        final Project project = projectDTOToProjectForSoap(projectDTO);
        final ProjectDeleteByIdResponse response = new ProjectDeleteByIdResponse();
        response.setProject(project);
        return response;
    }

    @ResponsePayload
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "ProjectDeleteRequest")
    public ProjectDeleteResponse deleteProject(@RequestPayload @NotNull final ProjectDeleteRequest request) throws Exception {
        final ProjectDTO projectDTO = projectDTOService.findOneById(request.getProject().getId());
        projectDTOService.remove(projectDTO);
        final Project project = projectDTOToProjectForSoap(projectDTO);
        final ProjectDeleteResponse response = new ProjectDeleteResponse();
        response.setProject(project);
        return response;
    }

    @ResponsePayload
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "ProjectByIdRequest")
    public ProjectByIdResponse findProjectById(@RequestPayload @NotNull final ProjectByIdRequest request) throws Exception {
        final ProjectDTO projectDTO = projectDTOService.findOneById(request.getId());
        final ProjectByIdResponse response = new ProjectByIdResponse();
        response.setProject(projectDTOToProjectForSoap(projectDTO));
        return response;
    }

    @ResponsePayload
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "ProjectUpdateRequest")
    public ProjectUpdateResponse updateProject(@RequestPayload @NotNull final ProjectUpdateRequest request) throws Exception {
        final ProjectDTO projectDTOToUpdate = new ProjectDTO();
        projectDTOToUpdate.setId(request.getProject().getId());
        projectDTOToUpdate.setName(request.getProject().getName());
        projectDTOToUpdate.setDescription(request.getProject().getDescription());
        final ProjectUpdateResponse response = new ProjectUpdateResponse();
        response.setProject(projectDTOToProjectForSoap(projectDTOService.update(projectDTOToUpdate)));
        return response;
    }

    @ResponsePayload
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "ProjectCountRequest")
    public ProjectCountResponse getProjectCount(@RequestPayload @NotNull final ProjectCountRequest request) throws Exception {
        final ProjectCountResponse response = new ProjectCountResponse();
        response.setCount(BigInteger.valueOf(projectDTOService.count()));
        return response;
    }

    private Project projectDTOToProjectForSoap(@NotNull final ProjectDTO projectDTO) {
        final Project project = new Project();
        project.setId(projectDTO.getId());
        project.setName(projectDTO.getName());
        project.setDescription(projectDTO.getDescription());
        return project;
    }

}
