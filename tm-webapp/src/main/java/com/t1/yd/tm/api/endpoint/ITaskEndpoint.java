package com.t1.yd.tm.api.endpoint;

import com.t1.yd.tm.dto.model.TaskDTO;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface ITaskEndpoint {

    @NotNull
    @WebMethod
    List<TaskDTO> findAll() throws Exception;

    @NotNull
    @WebMethod
    TaskDTO update(@WebParam(name = "task", partName = "task") @NotNull TaskDTO task) throws Exception;

    @Nullable
    @WebMethod
    TaskDTO findById(@WebParam(name = "id", partName = "id") @NotNull String id) throws Exception;

    @WebMethod
    boolean existsById(@WebParam(name = "id", partName = "id") @NotNull String id) throws Exception;

    @WebMethod
    long count() throws Exception;

    @WebMethod
    void deleteById(@WebParam(name = "id", partName = "id") @NotNull String id) throws Exception;

    @WebMethod
    void delete(@WebParam(name = "task", partName = "task") @NotNull TaskDTO task) throws Exception;

    void clear() throws Exception;

    @WebMethod
    List<TaskDTO> findByProjectId(@WebParam(name = "projectId", partName = "projectId") @NotNull final String projectId) throws Exception;

}
