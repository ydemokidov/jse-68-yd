package com.t1.yd.tm.controller;

import com.t1.yd.tm.api.service.IProjectService;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ProjectsController {

    private final IProjectService projectService;

    @Autowired
    public ProjectsController(@NotNull final IProjectService projectService) {
        this.projectService = projectService;
    }

    @GetMapping("/projects")
    public ModelAndView listProjects() throws Exception {
        return new ModelAndView("project-list", "projects", projectService.findAll());
    }

}
