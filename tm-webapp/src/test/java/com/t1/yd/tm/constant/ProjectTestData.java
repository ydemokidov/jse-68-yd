package com.t1.yd.tm.constant;

import org.jetbrains.annotations.NotNull;

public class ProjectTestData {

    @NotNull
    public final static String PROJECT1_NAME = "PROJECT1";

    @NotNull
    public final static String PROJECT1_DESCRIPTION = "PROJECT1_DESC";

    @NotNull
    public final static String PROJECT2_NAME = "PROJECT2";

    @NotNull
    public final static String PROJECT2_DESCRIPTION = "PROJECT2_DESC";

    @NotNull
    public final static String PROJECT3_NAME = "PROJECT3";

    @NotNull
    public final static String PROJECT3_DESCRIPTION = "PROJECT3_DESC";


}
