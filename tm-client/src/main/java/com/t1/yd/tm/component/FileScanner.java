package com.t1.yd.tm.component;

import com.t1.yd.tm.event.ConsoleEvent;
import com.t1.yd.tm.listener.AbstractListener;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Component
public final class FileScanner {

    @NotNull
    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();
    @NotNull
    private final List<String> commands = new ArrayList<>();

    @NotNull
    private final List<AbstractListener> listeners;
    @NotNull
    private final File folder = new File("/");

    @NotNull
    private ApplicationEventPublisher publisher;

    @Autowired
    public FileScanner(@NotNull final List<AbstractListener> listeners,
                       @NotNull final ApplicationEventPublisher publisher) {
        this.publisher = publisher;
        this.listeners = listeners;
    }

    @PostConstruct
    public void init() {
        final Iterable<AbstractListener> commands = listeners.stream()
                .filter(abstractListener -> abstractListener.getArgument() != null)
                .collect(Collectors.toList());
        commands.forEach(e -> this.commands.add(e.getName()));
        es.scheduleWithFixedDelay(this::process, 0, 3, TimeUnit.SECONDS);
    }

    private void process() {
        for (final File file : folder.listFiles()) {
            if (file.isDirectory()) continue;
            final String fileName = file.getName();
            final boolean check = commands.contains(fileName);
            if (check) {
                try {
                    file.delete();
                    publisher.publishEvent(new ConsoleEvent(fileName));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void stop() {
        es.shutdown();
    }

}
