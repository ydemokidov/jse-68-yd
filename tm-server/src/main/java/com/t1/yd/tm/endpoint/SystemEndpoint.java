package com.t1.yd.tm.endpoint;

import com.t1.yd.tm.api.endpoint.ISystemEndpoint;
import com.t1.yd.tm.api.service.IAuthService;
import com.t1.yd.tm.api.service.IPropertyService;
import com.t1.yd.tm.dto.request.system.ServerAboutRequest;
import com.t1.yd.tm.dto.request.system.ServerVersionRequest;
import com.t1.yd.tm.dto.response.system.ServerAboutResponse;
import com.t1.yd.tm.dto.response.system.ServerVersionResponse;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Controller
@NoArgsConstructor
@WebService(endpointInterface = "com.t1.yd.tm.api.endpoint.ISystemEndpoint")
public final class SystemEndpoint extends AbstractEndpoint implements ISystemEndpoint {

    private IPropertyService propertyService;

    @Autowired
    public SystemEndpoint(@NotNull final IAuthService authService,
                          @NotNull final IPropertyService propertyService) {
        super(authService);
        this.propertyService = propertyService;
    }

    @NotNull
    @Override
    @WebMethod
    public ServerAboutResponse getAbout(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final ServerAboutRequest request) {
        ;
        @NotNull final ServerAboutResponse response = new ServerAboutResponse();
        response.setEmail(propertyService.getAuthorEmail());
        response.setName(propertyService.getAuthorName());
        return response;
    }

    @NotNull
    @Override
    @WebMethod
    public ServerVersionResponse getVersion(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final ServerVersionRequest request) {
        @NotNull final ServerVersionResponse response = new ServerVersionResponse();
        response.setVersion(propertyService.getApplicationVersion());
        return response;
    }


}
