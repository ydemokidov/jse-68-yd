package com.t1.yd.tm.dto.response.task;

import com.t1.yd.tm.dto.model.TaskDTO;
import com.t1.yd.tm.dto.response.AbstractResultResponse;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public class AbstractTaskResponse extends AbstractResultResponse {

    @Nullable
    private TaskDTO taskDTO;

    public AbstractTaskResponse(@Nullable final TaskDTO taskDTO) {
        this.taskDTO = taskDTO;
    }

    public AbstractTaskResponse(@Nullable final Throwable throwable) {
        super(throwable);
    }

}
